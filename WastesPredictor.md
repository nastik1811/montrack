
# Применение технологий машинного обучения для расширения функциональности приложение

В проекте Montrack используется регрессионая модель Риджа, предсказывающая расходы на основе возраста и пола пользователя, а также количестве праздников в месяце.


```python
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
```

## Функции для генерирования данных


```python
def get_dummy_data(size):
    age = np.random.randint(16, 50, size)
    sex = np.random.randint(2, size=size)
    holidays = np.random.poisson(3, size) + 1
    wastes = generate_waste(age, sex, holidays)
    return {'age': age, 'sex': sex, 'holidays': holidays, 'wastes': wastes}

```


```python
# men spend more then women for presents
# wastes have linear correlation with holidays
# sum increases with age, but this growth slower then linear (log2 in my case) 
# sex: man - 1, woman - 0

def generate_waste(age, sex, holidays):
    holiday_k = 50
    age_k = 100
   
    return (0.7 + sex) * holiday_k * holidays + age_k * np.log2(age) + np.random.normal(50, 60)

```


```python
df = pd.DataFrame(get_dummy_data(100))
df
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>age</th>
      <th>sex</th>
      <th>holidays</th>
      <th>wastes</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>16</td>
      <td>1</td>
      <td>6</td>
      <td>943.886788</td>
    </tr>
    <tr>
      <th>1</th>
      <td>18</td>
      <td>0</td>
      <td>5</td>
      <td>625.879288</td>
    </tr>
    <tr>
      <th>2</th>
      <td>32</td>
      <td>0</td>
      <td>3</td>
      <td>638.886788</td>
    </tr>
    <tr>
      <th>3</th>
      <td>23</td>
      <td>0</td>
      <td>4</td>
      <td>626.242984</td>
    </tr>
    <tr>
      <th>4</th>
      <td>41</td>
      <td>0</td>
      <td>5</td>
      <td>744.641988</td>
    </tr>
    <tr>
      <th>5</th>
      <td>43</td>
      <td>0</td>
      <td>4</td>
      <td>716.513263</td>
    </tr>
    <tr>
      <th>6</th>
      <td>49</td>
      <td>1</td>
      <td>5</td>
      <td>1020.357772</td>
    </tr>
    <tr>
      <th>7</th>
      <td>25</td>
      <td>0</td>
      <td>4</td>
      <td>638.272407</td>
    </tr>
    <tr>
      <th>8</th>
      <td>16</td>
      <td>0</td>
      <td>5</td>
      <td>608.886788</td>
    </tr>
    <tr>
      <th>9</th>
      <td>21</td>
      <td>1</td>
      <td>4</td>
      <td>813.118530</td>
    </tr>
    <tr>
      <th>10</th>
      <td>37</td>
      <td>0</td>
      <td>3</td>
      <td>659.832125</td>
    </tr>
    <tr>
      <th>11</th>
      <td>47</td>
      <td>1</td>
      <td>4</td>
      <td>929.345673</td>
    </tr>
    <tr>
      <th>12</th>
      <td>37</td>
      <td>1</td>
      <td>3</td>
      <td>809.832125</td>
    </tr>
    <tr>
      <th>13</th>
      <td>16</td>
      <td>0</td>
      <td>5</td>
      <td>608.886788</td>
    </tr>
    <tr>
      <th>14</th>
      <td>39</td>
      <td>1</td>
      <td>3</td>
      <td>817.427010</td>
    </tr>
    <tr>
      <th>15</th>
      <td>20</td>
      <td>0</td>
      <td>3</td>
      <td>571.079597</td>
    </tr>
    <tr>
      <th>16</th>
      <td>38</td>
      <td>1</td>
      <td>2</td>
      <td>728.679539</td>
    </tr>
    <tr>
      <th>17</th>
      <td>40</td>
      <td>0</td>
      <td>2</td>
      <td>636.079597</td>
    </tr>
    <tr>
      <th>18</th>
      <td>32</td>
      <td>1</td>
      <td>1</td>
      <td>618.886788</td>
    </tr>
    <tr>
      <th>19</th>
      <td>16</td>
      <td>0</td>
      <td>3</td>
      <td>538.886788</td>
    </tr>
    <tr>
      <th>20</th>
      <td>39</td>
      <td>1</td>
      <td>4</td>
      <td>902.427010</td>
    </tr>
    <tr>
      <th>21</th>
      <td>47</td>
      <td>1</td>
      <td>4</td>
      <td>929.345673</td>
    </tr>
    <tr>
      <th>22</th>
      <td>37</td>
      <td>1</td>
      <td>5</td>
      <td>979.832125</td>
    </tr>
    <tr>
      <th>23</th>
      <td>31</td>
      <td>0</td>
      <td>5</td>
      <td>704.306419</td>
    </tr>
    <tr>
      <th>24</th>
      <td>49</td>
      <td>1</td>
      <td>5</td>
      <td>1020.357772</td>
    </tr>
    <tr>
      <th>25</th>
      <td>19</td>
      <td>0</td>
      <td>1</td>
      <td>493.679539</td>
    </tr>
    <tr>
      <th>26</th>
      <td>27</td>
      <td>1</td>
      <td>5</td>
      <td>934.375538</td>
    </tr>
    <tr>
      <th>27</th>
      <td>38</td>
      <td>1</td>
      <td>6</td>
      <td>1068.679539</td>
    </tr>
    <tr>
      <th>28</th>
      <td>24</td>
      <td>0</td>
      <td>3</td>
      <td>597.383038</td>
    </tr>
    <tr>
      <th>29</th>
      <td>44</td>
      <td>1</td>
      <td>5</td>
      <td>1004.829950</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>70</th>
      <td>35</td>
      <td>0</td>
      <td>5</td>
      <td>721.815090</td>
    </tr>
    <tr>
      <th>71</th>
      <td>37</td>
      <td>0</td>
      <td>4</td>
      <td>694.832125</td>
    </tr>
    <tr>
      <th>72</th>
      <td>44</td>
      <td>1</td>
      <td>5</td>
      <td>1004.829950</td>
    </tr>
    <tr>
      <th>73</th>
      <td>33</td>
      <td>1</td>
      <td>2</td>
      <td>708.326200</td>
    </tr>
    <tr>
      <th>74</th>
      <td>34</td>
      <td>0</td>
      <td>3</td>
      <td>647.633072</td>
    </tr>
    <tr>
      <th>75</th>
      <td>38</td>
      <td>0</td>
      <td>6</td>
      <td>768.679539</td>
    </tr>
    <tr>
      <th>76</th>
      <td>41</td>
      <td>0</td>
      <td>2</td>
      <td>639.641988</td>
    </tr>
    <tr>
      <th>77</th>
      <td>42</td>
      <td>1</td>
      <td>5</td>
      <td>998.118530</td>
    </tr>
    <tr>
      <th>78</th>
      <td>42</td>
      <td>1</td>
      <td>3</td>
      <td>828.118530</td>
    </tr>
    <tr>
      <th>79</th>
      <td>47</td>
      <td>1</td>
      <td>5</td>
      <td>1014.345673</td>
    </tr>
    <tr>
      <th>80</th>
      <td>40</td>
      <td>1</td>
      <td>3</td>
      <td>821.079597</td>
    </tr>
    <tr>
      <th>81</th>
      <td>24</td>
      <td>1</td>
      <td>8</td>
      <td>1172.383038</td>
    </tr>
    <tr>
      <th>82</th>
      <td>17</td>
      <td>0</td>
      <td>7</td>
      <td>687.633072</td>
    </tr>
    <tr>
      <th>83</th>
      <td>28</td>
      <td>0</td>
      <td>4</td>
      <td>654.622280</td>
    </tr>
    <tr>
      <th>84</th>
      <td>32</td>
      <td>0</td>
      <td>4</td>
      <td>673.886788</td>
    </tr>
    <tr>
      <th>85</th>
      <td>17</td>
      <td>0</td>
      <td>4</td>
      <td>582.633072</td>
    </tr>
    <tr>
      <th>86</th>
      <td>42</td>
      <td>0</td>
      <td>7</td>
      <td>818.118530</td>
    </tr>
    <tr>
      <th>87</th>
      <td>17</td>
      <td>0</td>
      <td>6</td>
      <td>652.633072</td>
    </tr>
    <tr>
      <th>88</th>
      <td>48</td>
      <td>1</td>
      <td>5</td>
      <td>1017.383038</td>
    </tr>
    <tr>
      <th>89</th>
      <td>33</td>
      <td>0</td>
      <td>3</td>
      <td>643.326200</td>
    </tr>
    <tr>
      <th>90</th>
      <td>17</td>
      <td>0</td>
      <td>2</td>
      <td>512.633072</td>
    </tr>
    <tr>
      <th>91</th>
      <td>29</td>
      <td>1</td>
      <td>3</td>
      <td>774.684887</td>
    </tr>
    <tr>
      <th>92</th>
      <td>23</td>
      <td>0</td>
      <td>3</td>
      <td>591.242984</td>
    </tr>
    <tr>
      <th>93</th>
      <td>35</td>
      <td>1</td>
      <td>9</td>
      <td>1311.815090</td>
    </tr>
    <tr>
      <th>94</th>
      <td>29</td>
      <td>1</td>
      <td>4</td>
      <td>859.684887</td>
    </tr>
    <tr>
      <th>95</th>
      <td>39</td>
      <td>0</td>
      <td>4</td>
      <td>702.427010</td>
    </tr>
    <tr>
      <th>96</th>
      <td>48</td>
      <td>0</td>
      <td>3</td>
      <td>697.383038</td>
    </tr>
    <tr>
      <th>97</th>
      <td>42</td>
      <td>1</td>
      <td>3</td>
      <td>828.118530</td>
    </tr>
    <tr>
      <th>98</th>
      <td>35</td>
      <td>0</td>
      <td>4</td>
      <td>686.815090</td>
    </tr>
    <tr>
      <th>99</th>
      <td>39</td>
      <td>0</td>
      <td>6</td>
      <td>772.427010</td>
    </tr>
  </tbody>
</table>
<p>100 rows × 4 columns</p>
</div>



## Создаём и обучаем регрессионную модель с помощью библиотеки __sklearn__


```python
from sklearn import linear_model
from sklearn.model_selection import train_test_split
```


```python
X = df[['age', 'sex', 'holidays' ]]
Y = df['wastes']
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25)

model = linear_model.Ridge(alpha=1.0)
model.fit(X_train, y_train)
```




    Ridge(alpha=1.0, copy_X=True, fit_intercept=True, max_iter=None,
          normalize=False, random_state=None, solver='auto', tol=0.001)



## Вычислим среднюю квадратичную ошибку и коэффициент детерминации для оценки нашей модели


```python
print(mean_squared_error(y_test, model.predict(X_test)))
print(r2_score(model.predict(X_test), y_test))
```

    1777.474152130849
    0.9418340675124613


### Выполним предсказание для одного сэмпла


```python
age = 20
sex = 0
holidays = 0
```


```python
model.predict(np.asarray([[age, sex, holidays]]))[0]
```




    363.81949777856016


