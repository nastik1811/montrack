from django.db import models
from django.contrib.auth.models import User
from PIL import Image


class Profile(models.Model):
    SEX = (
        (1, 'Male'),
        (0, 'Female')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.png', upload_to='profile_pics')
    age = models.IntegerField(blank=True, null=True)
    sex = models.IntegerField(default=0, choices=SEX)
    
    def __str__(self):
        return f'{self.user.username} Profile'

    def save(self, *args, **kwargs):
        super().save()
        try:
            img = Image.open(self.image.path)
        except(FileNotFoundError):
            return
    
        if img.height > 250 or img.width > 250:
            output_size = (250, 250)
            img.thumbnail(output_size)
            img.save(self.image.path)
            
