from django.urls import path
from . import views
from .views import (
    ExpensesListView,
    ExpenseCreateView,
    ReportCreateView,
    ReportListView,
    report_detail
)

urlpatterns = [
    path('overview/', ExpensesListView.as_view(), name='overview'),
    path('expense/new/', ExpenseCreateView.as_view(), name='new-expense'),
    path('report/<int:pk>/', report_detail, name='report-detail'),
    path('reports/new/', ReportCreateView.as_view(), name='create-report'),
    path('reports', ReportListView.as_view(), name='reports'),
]
