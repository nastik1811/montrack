import pandas as pd
import numpy as np
from sklearn import linear_model
from sklearn.model_selection import train_test_split

from tracker.utils.data_generator import get_dummy_data


class Predictor:
    def __init__(self):
        self.is_network_ready = False
        self.model = linear_model.Ridge(alpha=1.0)
    
    def train_network(self, data):
        df = pd.DataFrame(
            {'age': data['age'], 'sex': data['sex'], 'holidays': data['holidays'], 'wastes': data['wastes']})
        X = df[['age', 'sex', 'holidays']]
        Y = df['wastes']
        
        X_train, _, y_train, _ = train_test_split(X, Y, test_size=0.25)
        
        self.model.fit(X_train, y_train)
        self.is_network_ready = True
    
    def predict_waste(self, age, sex, holidays):
        if not self.is_network_ready:
            self.train_network(get_dummy_data(size=100))
        
        return self.model.predict(np.asarray([[age, sex, holidays]]))
