from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
from tracker.utils.currency_converter import convert_from_to

CURRENCY_CHOICES = (
    ('USD', '$'),
    ('EUR', '€'),
    ('BYR', 'б. р.')
)


class Category(models.Model):
    name = models.CharField(max_length=100)
    super_category = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE,
                                       related_name='categories')
    
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
    
    def __str__(self):
        return self.name


class Expense(models.Model):
    category = models.ForeignKey('Category', on_delete=models.CASCADE, related_name='category_expenses',
                                 related_query_name='category_expense')
    budget = models.ForeignKey('Budget', on_delete=models.CASCADE, related_name='expenses')
    date = models.DateField(default=timezone.now)
    amount = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)
    currency = models.CharField(max_length=5, choices=CURRENCY_CHOICES, default='BYR')
    
    # tags = models.ManyToManyField('Tag', related_name='+', blank=True)
    
    class Meta:
        verbose_name = 'Expense'
        verbose_name_plural = 'Expenses'
        ordering = ['-date']
    
    def __str__(self):
        return f'{self.category}_{self.date}'
    
    def get_absolute_url(self):
        return reverse('overview')
    
    def get_converted_amount(self):
        if self.currency == 'BYR':
            return self.amount
        return convert_from_to(self.currency, self.amount, self.date.__format__('%m/%d/%Y'))


class Budget(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    
    class Meta:
        verbose_name = 'Budget'
    
    def __str__(self):
        return f'{self.user.username} Budget'


class Tag(models.Model):
    name = models.CharField(max_length=150)
    default = models.NullBooleanField()
    
    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'
    
    def __str__(self):
        return self.name


class Report(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="reports")
    creation_date = models.DateField(auto_now_add=True)
    
    start_date = models.DateField(default=timezone.now)
    end_date = models.DateField(default=timezone.now)
    
    def __str__(self):
        return self.creation_date.__format__('%d/%m/%Y')
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    def get_absolute_url(self):
        return reverse('report-detail', kwargs={'pk': self.pk})
