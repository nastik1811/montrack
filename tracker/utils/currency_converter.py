from pymongo import MongoClient
from decimal import Decimal
import requests


DB_NAME = 'info'
COLLECTION_NAME = 'currencies'

currencies_list = MongoClient()[DB_NAME][COLLECTION_NAME]


def make_currency_request(date):
    usd_info = requests.get('http://www.nbrb.by/API/ExRates/Rates/USD',
                            params={'Periodicity': 0, 'ParamMode': 2, 'onDate': date}).json()
    euro_info = requests.get('http://www.nbrb.by/API/ExRates/Rates/EUR',
                             params={'Periodicity': 0, 'ParamMode': 2, 'onDate': date}).json()
    info = {
        'date': date,
        'USD': usd_info['Cur_OfficialRate'],
        'EUR': euro_info['Cur_OfficialRate']
    }
    return info


def get_actual_currency(date):
    actual_info = currencies_list.find_one({'date': date})
    if actual_info is None:
        actual_info = make_currency_request(date)
        currencies_list.insert_one(actual_info)
    return actual_info


def convert_from_to(init_cur, value, date):
    currencies = get_actual_currency(date)
    if init_cur == 'USD':
        c = currencies['USD']
    else:
        c = currencies['EUR']
    
    return value * Decimal(c)

