import numpy as np


def get_dummy_data(size):
    age = np.random.randint(16, 50, size)
    sex = np.random.randint(2, size=size)
    holidays = np.random.poisson(3, size) + 1
    wastes = generate_waste(age, sex, holidays)
    return {'age': age, 'sex': sex, 'holidays': holidays, 'wastes': wastes}


def generate_waste(age, sex, holidays):
    holiday_k = 50
    age_k = 100
    
    # return k * holidays + 10 * age + np.random.normal(50, 90) + sex * 100
    return (0.7 + sex) * holiday_k * holidays + age_k * np.log2(age) + np.random.normal(50, 60)
