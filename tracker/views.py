from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django import forms
from django.views.generic import (
    ListView,
    CreateView,
)
from .models import Category, Expense, Report
from bootstrap_datepicker_plus import DatePickerInput
from tracker.utils.data_processor import get_sums_by_categories, total_sum, month_sum, predict_wastes, date


class CategoryListView(ListView):
    model = Category
    template_name = 'tracker/overview.html'
    context_object_name = 'categories'


class ExpensesListView(LoginRequiredMixin, ListView):
    model = Expense
    template_name = 'tracker/overview.html'
    context_object_name = 'expenses'
    
    ordering = ['date']
    
    def get_queryset(self):
        return Expense.objects.filter(budget=self.request.user.budget)
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ExpensesListView, self).get_context_data(**kwargs)
        context.update({
            'reports': Report.objects.filter(user=self.request.user),
            'month_statistic': [month_sum(self.request.user, i, date.today().year) for i in range(1, 7)],
            'next_month': predict_wastes(self.request.user)
        })
        return context


class ExpenseCreateView(LoginRequiredMixin, CreateView):
    model = Expense
    template_name = 'tracker/new_expense.html'
    context_object_name = 'expenses'
    fields = ['date', 'category', 'amount', 'currency']
    
    widgets = {
        'date': DatePickerInput(format='%d/%m/%Y'),  # python date-time format
    }
    
    def form_valid(self, form):
        form.instance.budget = self.request.user.budget
        return super().form_valid(form)


class ReportCreateForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ['start_date', 'end_date']
        widgets = {
            'start_date': DatePickerInput().start_of('event days'),
            'end_date': DatePickerInput().end_of('event days'),
        }


class ReportCreateView(LoginRequiredMixin, CreateView):
    model = Report
    template_name = 'tracker/new_report.html'
    form_class = ReportCreateForm
    
    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class ReportListView(LoginRequiredMixin, ListView):
    model = Report
    template_name = 'tracker/reports.html'
    context_object_name = 'reports'
    
    def get_queryset(self):
        return Report.objects.filter(user=self.request.user)


def report_detail(request, pk):
    rep = Report.objects.get(pk=pk)
    user = request.user
    
    context = {
        'title': rep.creation_date,
        'from': rep.start_date,
        'to': rep.end_date,
        'expenses': get_sums_by_categories(user, rep.start_date, rep.end_date),
        'total_sum': total_sum(user, rep.start_date, rep.end_date)
    }
    
    return render(request, 'tracker/report_detail.html', context=context)

