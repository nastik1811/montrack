from datetime import date
from django.contrib.auth.models import User
from tracker.models import Category
from .wastes_predictor import Predictor

predictor = Predictor()


def collect_month_data(month, year):
    age = []
    sex = []
    wastes = []
    
    if month < 12:
        next_month = month + 1
    else:
        next_month = 1
        year += 1

    holidays = [holiday_number(month)] * User.objects.all().count()
    
    for user in User.objects.all():
        usr_wastes = total_sum(user, date(year, month, 1), date(year, next_month, 1))
        usr_age = user.profile.age if user.profile.age else 18
        usr_sex = user.profile.sex
        
        wastes.append(usr_wastes)
        age.append(usr_age)
        sex.append(usr_sex)
    
    return {'age': age, 'sex': sex, 'holidays': holidays, 'wastes': wastes}


def collect_year_data(year):
    data = {'age':[], 'sex': [], 'holidays': [], 'wastes': []}
    for i in range(12):
        month_data = collect_month_data(i, year)
        data.update()
    
        
def predict_wastes(user):
    
    if date.today().month < 12:
        next_month = date.today().month + 1
    else:
        next_month = 1
    usr_age = user.profile.age if user.profile.age else 18
    return predictor.predict_waste(usr_age, user.profile.sex, holiday_number(next_month))
    

def holiday_number(month):
    month_holidays = {
        1: 5,
        2: 3,
        3: 4,
        4: 3,
        5: 8,
        6: 2,
        7: 3,
        8: 6,
        9: 4,
        10: 4,
        11: 5,
        12: 7
    }
    return month_holidays[month]


def get_category_sum(expenses):
    return sum([e.get_converted_amount() for e in expenses])


def get_sums_by_categories(user, start, end):
    expenses_dict = {
        cat.name: get_category_sum(list(cat.category_expenses.all()
                                        .filter(budget=user.budget)
                                        .filter(date__range=[start, end]))
                                   )
        for cat in Category.objects.all()}
    return expenses_dict


def total_sum(user, start, end):
    sums_by_categories = get_sums_by_categories(user, start, end)
    
    return sum([s for s in sums_by_categories.values()])


def month_sum(user, month, year):
    if month < 12:
        next_month = month + 1
    else:
        next_month = 1
        year += 1
        
    return total_sum(user, date(year, month, 1), date(year, next_month, 1))

